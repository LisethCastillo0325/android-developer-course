package com.example.courseapps

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.courseapps.ui.theme.CourseAppsTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CourseAppsTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    ComposableScreen()
                }
            }
        }
    }
}


@Composable
fun ComposableCard(title: String, description: String, modifier: Modifier = Modifier) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
            .padding(16.dp)
            .fillMaxSize()
    ) {
        Text(
            text = title,
            fontWeight = FontWeight.Bold,
            modifier = Modifier
                .padding(16.dp)
                .align(alignment = Alignment.CenterHorizontally)
        )
        Text(
            text = description,
            textAlign = TextAlign.Justify
        )
    }
}

@Composable
fun ComposableScreen(modifier: Modifier = Modifier) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        Row(
            modifier = Modifier
                .weight(1f)
        ) {
            ComposableCard(
                title = stringResource(R.string.text_composable),
                description = stringResource(R.string.text_composable_description),
                modifier = Modifier
                    .weight(1f)
                    .background(Color(0xFFEADDFF))
            )
            ComposableCard(
                title = stringResource(R.string.image_composable),
                description = stringResource(R.string.image_composable_description),
                modifier = Modifier
                    .weight(1f)
                    .background(Color(0xFFD0BCFF))
            )
        }
        Row(
            modifier = Modifier
                .weight(1f)
        ) {
            ComposableCard(
                title = stringResource(R.string.row_composable),
                description = stringResource(R.string.row_composable_description),
                modifier = Modifier
                    .weight(1f)
                    .background(Color(0xFFB69DF8))
            )
            ComposableCard(
                title = stringResource(R.string.column_composable),
                description = stringResource(R.string.column_composable_description),
                modifier = Modifier
                    .weight(1f)
                    .background(Color(0xFFF6EDFF))
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    CourseAppsTheme {
        ComposableScreen()
    }
}